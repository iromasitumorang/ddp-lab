# Repository Tutorial & Lab
Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Gasal 2018/2019
***


## Daftar Isi

Repository ini akan berisi materi-materi Tutorial & Lab DDP 2.

1. Lab
    1. [`Lab 1 : Pengenalan Java dan Git`][Lab_1] 
    2. [`Lab 2 : Expression, Operators, dan Decision`][Lab_2]
    3. [`Lab 3 : Iteration`][Lab_3]
    4. [`Lab 4 : Pengenalan OOP`][Lab_4]
    5. [`Lab 5 : Array dan ArrayList`][Lab_5]
    
_Tools_ yang akan digunakan dalam mata kuliah ini antara lain :

- Java Development Kit (JDK) 8
- Git
- Notepad++ (atau text editor sejenisnya)
- Integrated Development Environment (IDE)
- Gradle
- GitLab Account

Pastikan kalian telah menginstall / memiliki _tools_ diatas, jika belum bisa melihat petunjuk penginstallan dan
konfigurasinya [disini](https://drive.google.com/file/d/1c1AA-9ju1S82-NYyV7EMyPNwScPpMQsr/view?usp=sharing)

[Lab_1]: lab_instructions/lab_1/README.md
[Lab_2]: lab_instructions/lab_2/README.md
[Lab_3]: lab_instructions/lab_3/README.md
[Lab_4]: lab_instructions/lab_4/README.md
[Lab_5]: lab_instructions/lab_5/README.md